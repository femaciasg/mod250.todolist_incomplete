package mod250.toDoList.action;

import java.util.Collection;

import org.apache.struts.action.Action;

import mod250.toDoList.bean.Task;
import mod250.toDoList.dao.TaskDAO;

public class ListTasks extends Action {
	public org.apache.struts.action.ActionForward execute(
			org.apache.struts.action.ActionMapping mapping,
			org.apache.struts.action.ActionForm form,
			javax.servlet.http.HttpServletRequest request,
			javax.servlet.http.HttpServletResponse response) throws Exception {
	
		
		TaskDAO taskDAO = TaskDAO.getInstance();
		Collection<Task> listTasks = taskDAO.getAll();
		
		request.setAttribute("listTasks", listTasks);
		
		return mapping.findForward("success");
	}
}
