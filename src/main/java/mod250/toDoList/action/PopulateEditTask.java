package mod250.toDoList.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mod250.toDoList.bean.Task;
import mod250.toDoList.dao.TaskDAO;
import mod250.toDoList.form.TaskForm;

public class PopulateEditTask extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Integer taskId = new Integer(request.getParameter("id"));
		
		TaskDAO taskDAO = TaskDAO.getInstance();
		Task task = taskDAO.getById(taskId);
		
		TaskForm taskForm = (TaskForm) form;
		taskForm.setId(task.getId());
		taskForm.setTitle(task.getTitle());
		taskForm.setDescription(task.getDescription());
		
		return mapping.findForward("success");
		
	}
	
}
