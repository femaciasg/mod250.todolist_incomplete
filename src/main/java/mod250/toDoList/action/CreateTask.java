package mod250.toDoList.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mod250.toDoList.bean.Task;
import mod250.toDoList.dao.TaskDAO;
import mod250.toDoList.form.TaskForm;

public class CreateTask extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		TaskForm taskForm = (TaskForm) form;
		
		Task task = new Task();
		task.setTitle(taskForm.getTitle());
		task.setDescription(taskForm.getDescription());
		
		TaskDAO taskDAO = TaskDAO.getInstance();
		taskDAO.create(task);
		
		return mapping.findForward("success");
		
	}

}
