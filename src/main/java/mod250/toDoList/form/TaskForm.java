package mod250.toDoList.form;

import org.apache.struts.action.ActionForm;

public class TaskForm extends ActionForm {
	
	private static final long serialVersionUID = 3442846809513724379L;
	
	private int id;
	private String title;
	private String description;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}	
	
}
