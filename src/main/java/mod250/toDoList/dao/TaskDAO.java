package mod250.toDoList.dao;


import java.util.ArrayList;
import java.util.Collection;

import mod250.toDoList.bean.Task;

public class TaskDAO {
	
	private static TaskDAO INSTANCE = new TaskDAO();
	private int nextId;
	private ArrayList<Task> listTasks;
	
	
	private TaskDAO() {
		listTasks = new ArrayList<Task>();
		Task task = new Task();
		
		task.setId(0);
		task.setTitle("Task 1");
		task.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur purus odio, dapibus quis arcu at, tincidunt ornare orci. Phasellus viverra, neque eu sodales ultrices, augue sem tempus nisl, vitae congue arcu orci vitae velit. Duis quis vulputate purus. Donec mattis mi eget orci tristique imperdiet. Etiam id massa metus. Suspendisse.");
		listTasks.add(task);
		
		task = new Task();
		task.setId(1);
		task.setTitle("Task 2");
		task.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla lobortis ultricies. Mauris dictum felis eu posuere ultrices. Curabitur auctor, eros quis malesuada dapibus, quam enim ultrices ligula, vitae porttitor erat leo et mi. Sed a placerat erat, fringilla pretium nunc. Nam commodo libero sit amet turpis ornare, fringilla rhoncus libero lacinia. Curabitur non justo vitae dui blandit euismod. In id enim non tellus consequat malesuada nec ut turpis. Nunc imperdiet est eu elit auctor, vel semper quam scelerisque. Cras pharetra tincidunt lorem eu laoreet. Nunc sollicitudin at nunc non consequat.\n"
										+"Mauris luctus pellentesque mauris non placerat. Suspendisse mattis ante eget ipsum elementum bibendum. Sed in nulla et eros porta gravida. Aenean nec malesuada ligula. Proin sodales felis sapien, eget euismod sem volutpat ut. Vestibulum sit amet viverra lorem, vel tincidunt nisl. Proin suscipit felis quis dui gravida suscipit. Vivamus lobortis, enim sed condimentum aliquam, odio mi pellentesque justo, et.");
		listTasks.add(task);
		
		task = new Task();
		task.setId(2);
		task.setTitle("Task 3");
		task.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vehicula molestie diam, in eleifend odio vestibulum at. Phasellus vitae venenatis diam. Donec sed eros sagittis, malesuada sapien nec, placerat velit. Morbi quis rhoncus dui, ut tincidunt erat. Donec egestas nibh eget tellus lobortis consequat. Mauris a placerat tortor. Nunc ultricies felis urna. Suspendisse bibendum elementum viverra. Sed risus metus, posuere at justo a, ultrices fringilla metus. Mauris convallis elit in turpis euismod lobortis. Nam tincidunt varius quam eget consectetur. Etiam varius enim volutpat lacus consectetur, ac semper erat elementum. Aliquam urna ante, blandit quis dolor id, faucibus posuere nisi. Curabitur.");
		listTasks.add(task);
		
		nextId = 3;
	}
	
	public static TaskDAO getInstance() {
		return INSTANCE;
	}
	
	/**
	  * Retrieve in the task list
	  *
	  * @param Id of the task to retrieve
	  */
	public Task getById(Integer id) {
		Task task = new Task();
		task.setId(id);
		int index = listTasks.indexOf(task);
		if(index==-1) {
			return null;
		}
		else {
			return listTasks.get(index);
		}
	}
	
	/**
	  * Retrieve the full task list
	  */
	public Collection<Task> getAll() {
		return listTasks;
	}
	
	/**
	  * Insert a task element in the task list
	  *
	  * @param task Task to insert
	  */
	public void create(Task task) {
		task.setId(nextId);
		listTasks.add(task);
		nextId++;
	}
	
	/**
	  * Update an element in the task list
	  *
	  * @param task Task to update
	  */
	public void update(Task task) {
		int index = listTasks.indexOf(task);
		Task taskOld = listTasks.get(index);
		taskOld.setTitle(task.getTitle());
		taskOld.setDescription(task.getDescription());
	}
	
	/**
	  * Delete an element from the task list
	  *
	  * @param Id of the task to delete
	  */
	public void delete(Integer id) {
		Task task = new Task();
		task.setId(id);
		listTasks.remove(task);
	}
	
}
