<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/style.css">
<title>ToDoList - MOD250</title>
</head>
<body>
	<div class="container">
		<div class="containerHead" >
			ToDoList
			<html:link action="/listTasks">
				<img alt="Back" src="img/back.png">
			</html:link>
		</div>
		<div class="containerBody">
			<html:form action="/createTask" focus="title">
				<div class="task">
					Title
					<div class="taskTitle"><html:text property="title"/></div>
					Description
					<div class="taskDescription"><html:textarea property="description"/></div>
					<div><html:image alt="Save task" src="img/save.png"></html:image></div>
				</div>
			</html:form>
		</div>
		<div class="containerFoot">
			<img alt="HiB Logo" src="img/logo.png">
		</div>
	</div>

</body>
</html>