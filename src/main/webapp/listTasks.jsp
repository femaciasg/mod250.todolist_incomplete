<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/style.css">
<title>ToDoList - MOD250</title>
</head>
<body>
	<div class="container">
		<div class="containerHead" >
			ToDoList
			<html:link href="createTask.jsp">
				<img alt="New task" src="img/add.png">
			</html:link>
		</div>
		
		<div class="containerBody">
			<logic:iterate id="task" name="listTasks">
				<div class="task">
					<div class="taskTitle"><bean:write name="task" property="title"/></div>
					<div class="taskDescription"><bean:write name="task" property="description"/></div>
					<html:link action="/populateEditTask" paramName="task" paramProperty="id" paramId="id"><img alt="Edit task" src="img/edit.png"></html:link>
					<html:link action="/deleteTask" paramName="task" paramProperty="id" paramId="id"><img alt="borrar tarea" src="img/delete.png"></html:link>
				</div>
			</logic:iterate>
		</div>
		
		<div class="containerFoot">
			<img alt="HiB Logo" src="img/logo.png">
		</div>
	</div>
</body>
</html>