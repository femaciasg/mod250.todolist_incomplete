package mod250.toDoList.dao;

import mod250.toDoList.bean.Task;
import mod250.toDoList.dao.TaskDAO;

import org.junit.Test;

import static org.junit.Assert.*;


public class TaskDAOTest {
	
	@Test
	public void testGetById (){
		TaskDAO taskDAO = TaskDAO.getInstance();
		
		Task expectedTask = new Task();
		expectedTask.setId(0);
		expectedTask.setTitle("Task 1");
		expectedTask.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur purus odio, dapibus quis arcu at, tincidunt ornare orci. Phasellus viverra, neque eu sodales ultrices, augue sem tempus nisl, vitae congue arcu orci vitae velit. Duis quis vulputate purus. Donec mattis mi eget orci tristique imperdiet. Etiam id massa metus. Suspendisse.");
				
		Task obtainedTask = taskDAO.getById(0);
		
		assertEquals(expectedTask, obtainedTask);
	}
	
	@Test
	public void testDelete (){
		TaskDAO taskDAO = TaskDAO.getInstance();
		
		assertNotEquals(null, taskDAO.getById(0));
		taskDAO.delete(0);
		assertEquals(null, taskDAO.getById(0));
	}
}
